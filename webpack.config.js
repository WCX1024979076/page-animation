const path = require('path');

module.exports = {
  mode: 'development',
  entry: {
    app: './src'
  },
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      }
    ]
  },
  // devServer选项
  devServer: {
    static: path.join(__dirname, 'dist'),
    compress: true,
    port: 9000
  }
}
